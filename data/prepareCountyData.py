# Semi-Automated Mapping of Ohio Mosses
# Script name: prepareCountyData.py
# Created by: Center for Urban and Regional Analysis (OSU)
# Created for: Museum of Biological Diversity (OSU)
#
# Using county boundaries downloaded from ODOT TIMS (1), create shapefiles
# of county boundaries and county centroids, both in WGS 1984
#
# (1) https://gis.dot.state.oh.us/tims/Data/Download

import geopandas
import os


path = os.path.normpath("./county_boundaries")
if(not os.path.exists(path)):
	os.makedirs(path)

path = os.path.normpath("./county_centroids")
if(not os.path.exists(path)):
	os.makedirs(path)

counties = geopandas.read_file(os.path.normpath("./ODOT/REFER_COUNTY.shp"))

# County names are all caps.  Convert them to initial caps for compatibility with moss atlas data.
counties["COUNTY"] = [county.title() for county in counties["COUNTY"]]

# County name header is all caps.  Convert it to initial caps for compatibility with moss atlas data.
counties.rename(columns={'COUNTY': 'County'}, inplace=True)

if(counties.crs['init'] != "epsg:4326"):
    counties = counties.to_crs("epsg:4326")

centroids = geopandas.GeoDataFrame(counties[['County']])

centroids.crs = counties.crs
centroids['geometry'] = counties['geometry'].centroid

counties.to_file(os.path.normpath("./county_boundaries/county_boundaries.shp"))

centroids.to_file(os.path.normpath("county_centroids/county_centroids.shp"))