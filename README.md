# Semi-Automated Mapping of Ohio Mosses

Created by: Center for Urban and Regional Analysis (OSU)

Created for: Museum of Biological Diversity (OSU)

# Overview

This script automatically produces a set of 300+ maps showing the distribution of moss species in Ohio Counties.  The maps are intended for inclusion in the Ohio Moss and Lichen Association [Moss Atlas](https://ohiomosslichen.org/ohio_mosslist/).  The input to the script is a massive spreadsheet that captures the distribution of each species as observed by an authority on the subject.

# Instructions (command line) <a href="command_line"></a>

  1. Prepare the data in accordance with the [input data specifications](#input_data).
  1. Open the project file in ArcGIS Pro (`MossMapping.aprx` or customized file).
      - Note: You can skip this step if ArcGIS Pro is licensed for offline use
  1. Open command prompt
  1. Change to the directory where the script is located
  1. Run the following command:

  `"%PROGRAMFILES%\ArcGIS\Pro\bin\Python\Scripts\propy" MossMapping.py <inputFile> <outputFolder> <fileType> <projectFile>`

  where:
    - `<inputFile>` (required) is the path to the CSV or XLSX file containing the moss data to be mapped (see samples in `./sample_data/` folder)
    - `<outputFolder` (optional) is the path to the folder where you would like the output data to be stored.  It will be created if it does not exist (default=`./output`)
    - `<fileType>` (optional) is the file type of maps you would like to generate. You may select `JPG`, `PNG`, or `PDF`  (default=`JPG`)
    - `<projectFile>` (optional) is an ArcGIS project file (.aprx) with a custom layout.  (default=`MossMapping.aprx`)

  1. Observe the output from the program as it executes.  The script will take several minutes to generate the first map and several seconds per additional map. Address any errors and try again if necessary.

# Instructions (ArcGIS Pro Python tool)

  1. Prepare the data in accordance with the [input data specifications](#input_data).
  1. Open the project file in ArcGIS Pro (`MossMapping.aprx` or customized file).
  1. Open `MossMapping.tbx` toolbox
  1. Open the `MakeMossMaps` script tool.
  1. Specify the tool parameters:
    - `Moss atlas spreadsheet file` (required) is the path to the CSV file containing the moss data to be mapped (see samples in `./sample_data/` folder)
      - Note that only CSV is supported.  This is a limitation of the ArcGIS file selector.  If you want to map an XLSX file, use the [command line](#command_line) instructions.
    - `Output folder` (optional) is the path to the folder where you would like the output data to be stored.  It will be created if it does not exist (default=`./output`)
    - `Output file type` (optional) is the file type of maps you would like to generate. You may select `JPG`, `PNG`, or `PDF`  (default=`JPG`)
    - `Template project file` (optional) is an ArcGIS project file (.aprx) with a custom layout.  (default=`MossMapping.aprx`)
  1. Click `Run`
  1. Observe the output from the program as it executes.  The script will take several minutes to generate the first map and several seconds per additional map. Address any errors and try again if necessary.

**Known issue: When run as a script tool within ArcGIS, the script may not properly symbolize the moss presence.  Instead of displaying filled squares and circles to represent the presence of moss species, the map will display colored circles at the county centroids.  This does not seem to be a bug in the script, but rather a problem in the ArcGIS Python environment that prevents the `mosses_sym.renderer.fields` attribute from being set.  See additional details in the comments in `MossMapping.py`).  If the ArcGIS script tool fails, it will be necessary run the script from [the command line](#command_line).**

# Input data specification <a href="input_data"></a>

The script accepts a moss atlas in tabular form.  Each row in the table describes the presence of a moss species in Ohio counties according to a specified authority.

The script can accept input data in comma-separated value (.csv) or Excel (.xlsx) format. The format is detected automatically.  The spreadsheet (in either format) must have the following fields.  The field names are case sensitive and must be entered exactly as shown.

Field name | Field type | Description
---------- | ---------- | -----------
ScientificName | String | a species of moss
Authority | String | an author or list of authors
family | String | the taxonomic family of the moss
synonym | String | a well-known synonym for the scientific name
Adams | String | Moss presence in Adams county (1 of 88)
Allen | String | Moss presence in Allen county (2 of 88)
...   | String | 85 fields indicating moss presence in additional counties
Wyandot | String | Moss presence in Wyandot county (88 of 88)

Sample data files are available in `./sample_data`:
  - [CSV](./sample_data/moss_atlas.csv)
  - [Excel](./sample_data/moss_atlas.xlsx)

Notes:
  - The cells in the fields labeled with county names should contain one of the following values:
    - 1 (Specimen records, indicated by circle symbol on map)
    - 2 (Literature records only, indicated by square symbol on map)
    - Blank/empty (no data available)

# Supporting data

The following supporting datasets are required to run the script.  See comments in source code for details.
  - Ohio County Boundaries. Ohio Dept. of Transportation.  https://gis.dot.state.oh.us/tims/Data/Download

# Sample output

See contents of `sample_output` folder

# Customizing output

Charateristic | File | Variable or location | Comments
---------- | ---------- | ----------- | --------------
Moss presence symbol | MossMapping.py | itm.symbol.xxxx | Shape, size, fill color, outline color
County boundary symbol | MossMapping.py | boundaries_sym.renderer.symbol.xxxx | Style, size (thickness)
Text element styles | MossMapping.aprx | Look at style and properties for each element | Font type, style, and size.  Also anchor point, borders, background fill, etc.
Text element sizes | Not applicable | Not applicable | Text element size expands to fit contents
Element positions | MossMapping.py | TITLE_X0, TITLE_Y0, SPACER | Match units used in layout (default unit is inches).  X position is measured from left edge.  Y position is measured from bottom edge.
Map projection (spatial reference) | MossMapping.aprx | Map properties > Coordinate Systems | Default spatial reference is EPSG:3402 (Ohio State Plan South - US Feet)
Map extent | MossMapping.aprx | mf.camera.setExtent() | By default, MossMapping.py computes the extent automatically based on county polygons.  Alter it here, or comment out this line and change it in the Map Frame properties.
Text wrapping for first title line | MossMapping.py | TITLE_MAXCHAR | This is a bit complicated.  Study the code before making changes.
