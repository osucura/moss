# Semi-Automated Mapping of Ohio Mosses
# Script name: standardizeMossTables.py
# Created by: Center for Urban and Regional Analysis (OSU)
# Created for: Museum of Biological Diversity (OSU)
#
# Using the moss atlas as input, this script creates two tables in the form of CSV files:
#   - species_file contains sets in the form {species, authority} that identify Ohio moss species 
#     as studied by the authority.  Each set is assigned a numerical index.  The moss family is also
#     included in each case.
#   - counties_file provides crossreferences Ohio counties to the moss species found in each county.  The
#     county name field serves as the index.  The field names are numerical values that correspond to
#	  indices of the entries in species_file.

import pandas as pd
import os
import sys
import unicodedata
import re

def standardizeMossTables(in_file, out_folder):

    # The following two files will be created in out_folder
	COUNTIES_FILE = "moss_atlas_counties.csv"
	SPECIES_FILE = "moss_atlas_species.csv"

	# SPECIES_ATTR contains a list of field names in the moss atlas that are associated with each species
	SPECIES_ATTR = ["ScientificName","Authority","family","synonym"]

	# SPECIES_INDEX_NAME and COUNTY_INDEX_NAME are the field names that will be applied to the index field in
	# species_file and counties_file respectively
	SPECIES_INDEX_NAME = "ID"
	COUNTY_INDEX_NAME = "COUNTY"

	in_file = os.path.normpath(in_file)
	out_folder = os.path.normpath(out_folder)

	if(in_file.split(".")[-1].lower() == "xlsx" ):
		print("Detected XLSX input file")
		df = pd.read_excel(in_file)
	elif(in_file.split(".")[-1].lower() == "csv" ):
		print("Detected CSV input file")
		try:
			df = pd.read_csv(in_file, encoding = "utf-8")
		except UnicodeDecodeError: 
			try:
				df = pd.read_csv(in_file, encoding = "ISO-8859-1")
			except UnicodeDecodeError:
				print("ERROR: Unable to decode one or more characters in the input file.")
				sys.exit(-1)
	else:
		print("Unknown input file type")
		sys.exit(-1)
	
	# Remove summary line indicated by "ZZ-alldots"
	df = df[df['ScientificName'] != "ZZ-alldots"]

	id = ["SPECIES_" + str(id) for id in list(range(df.shape[0]))]
	
	species = df[SPECIES_ATTR].copy(deep=True)
	species[SPECIES_INDEX_NAME] = id 
	species.set_index(SPECIES_INDEX_NAME, inplace=True)


	temp = df.copy(deep=True)

	temp.drop(SPECIES_ATTR, axis=1, inplace=True)

	counties = temp.transpose()
	counties.columns = id
	counties.index.name = COUNTY_INDEX_NAME

	counties_file = os.path.join(out_folder, COUNTIES_FILE)
	species_file = os.path.join(out_folder, SPECIES_FILE)

	counties.to_csv(counties_file)
	species.to_csv(species_file)
	
	return(species_file, counties_file)
	
	
# Formats an arbitrary string for use as a filename or URL.  Borrowed from Django.  
# See code description: https://docs.djangoproject.com/en/2.1/ref/utils/#django.utils.text.slugify
# Use subject to Django license (included below)
def slugify(value, allow_unicode=False):
    """
    Convert to ASCII if 'allow_unicode' is False. Convert spaces or repeated
    dashes to single dashes. Remove characters that aren't alphanumerics,
    underscores, or hyphens. Convert to lowercase. Also strip leading and
    trailing whitespace, dashes, and underscores.
    """
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalize('NFKC', value)
    else:
        value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub(r'[^\w\s-]', '', value.lower())
    return re.sub(r'[-\s]+', '-', value).strip('-_')
	
# Copyright (c) Django Software Foundation and individual contributors.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
#     1. Redistributions of source code must retain the above copyright notice,
#        this list of conditions and the following disclaimer.
#
#     2. Redistributions in binary form must reproduce the above copyright
#        notice, this list of conditions and the following disclaimer in the
#        documentation and/or other materials provided with the distribution.
#
#     3. Neither the name of Django nor the names of its contributors may be used
#        to endorse or promote products derived from this software without
#        specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.