# Semi-Automated Mapping of Ohio Mosses
# Script name: MossMapping.py
# Created by: Center for Urban and Regional Analysis (OSU)
# Created for: Museum of Biological Diversity (OSU)
#
# KNOWN ISSUE: This script cannot be run as an ArcGIS Pro script tool.  The
# tool finishes successfully, however the moss presence is not symbolized
# properly.  This is because the mosses_sym.renderer.fields assignment fails.
# The cause of this is unknown (it works properly when script is run from
# the command line).

import arcpy
import os
import sys
import shutil
import pandas as pd
import MossUtil

### CUSTOMIZATION OPTIONS ##################################################################################

# Default path for all files created by script (can be overridden by input parameter)
DEFAULT_OUTPUT_FOLDER = os.path.normpath("./output")

# Default ArcGIS Pro project file containing template layout (can be overridden by input parameter)
DEFAULT_APRX = os.path.normpath("./MossMapping.aprx")

# Default map image output format.  Valid types are "JPG", "PNG", and "PDF".  (can be overridden by input parameter)
DEFAULT_FILETYPE = "JPG"

# Vertical distance from bottom of map image to first title line.  Specify value using the units used in the template layout
# in the ArcGIS project file.  Tip: the Y axis runs upward from the bottom left corner.
TITLE_Y0 = 1.25

# Number of characters allowed in the first title line before breaking the line.  Line will be broken at the first space reached
# prior to this limit. Horizontal positioning of the first title line relies on this value.  For best results set this value 
# such that the maximum number of characters occupies nearly all of the map image width.
TITLE_MAXCHAR = 58

### CONSTANTS (do not modify) ##############################################################################

# Reference dataset containing county centroids
CENTROIDS_SHP = os.path.normpath("./data/county_centroids/county_centroids.shp")

# Reference dataset containing county boundary polygons
BOUNDARIES_SHP = os.path.normpath("./data/county_boundaries/county_boundaries.shp")

# Geodatabase where spatial data will be stored
OUTPUT_GDB = "output.gdb"

# Table where species attributes (scientific name, synonym, authority, family) are stored in output geodatabase
SPECIES_TABLE = "species"

# Index field for species table
SPECIES_INDEX_NAME = "ID"

# Table where moss species presence by county is stored in output geodatabase
COUNTIES_TABLE = "counties"

# Index field for counties table
COUNTY_INDEX_NAME = "County"

# Point feature class representing county centroids.  Species presence attributes are joined to this to symbolize 
# moss distribution.
MOSSES_FC = "mosses"

# Polygon feature class representing county boundaries (included for completeness only)
BOUNDARIES_FC = "boundaries"

### MAIN SCRIPT #############################################################################################

# Get input file path. This is the only required parameter.  Try first to get it from ArcGIS Pro. If it 
# succeeds, get all remaining arguments from ArcGIS Pro. If it fails, get remaining arguments from the 
# command line.  
inputCsv = arcpy.GetParameterAsText(0).strip()
if(inputCsv != ""):
	outputFolder = arcpy.GetParameterAsText(1).strip()
	fileType = arcpy.GetParameterAsText(2).strip()
	projectFile = arcpy.GetParameterAsText(3).strip()
else:
	try: 
		inputCsv = os.path.normpath(sys.argv[1])
	except IndexError:
		arcpy.AddError("Moss atlas spreadsheet file is undefined.")
		sys.exit(-1)

	try:
		outputFolder = os.path.normpath(sys.argv[2])
	except IndexError:
		outputFolder = ""

	try: 
		fileType = os.path.normpath(sys.argv[3])
	except IndexError:
		fileType = ""

	try: 
		projectFile = os.path.normpath(sys.argv[4])
	except IndexError:
		projectFile = ""

arcpy.AddMessage("Using input file: " + inputCsv)

# If any of the optional parameters are still undefined, set them to their
# default values
if(outputFolder == ""):
	outputFolder = DEFAULT_OUTPUT_FOLDER
	arcpy.AddMessage("Output path is not specified. Using default path: " + outputFolder)
	
if(fileType == ""):
	fileType = DEFAULT_FILETYPE
	arcpy.AddMessage("Output file type is not specified.  Using default: " + fileType)
	
if(projectFile == ""):
	projectFile = DEFAULT_APRX
	arcpy.AddMessage("Project (APRX) file is not specified.  Using default: " + projectFile)

if (not os.path.exists(outputFolder)):
	arcpy.AddMessage("Specified output path does not exist. Attempting to create it recursively.")
	try:
		os.makedirs(outputFolder)
	except:
		arcpy.AddError("Failed to create output path.")
		sys.exit(-1)

# Native format of moss atlas is not conducive to manipulation by ArcGIS.
# MossUtil.standardizeMossTables indexes the moss species distribution by
# county and moves the moss attributes to a new table. This is accomplished
# using GeoPandas, so the resulting tables are saved as CSV for easy import
# into ArcGIS
arcpy.AddMessage("Creating intermediate CSV files for use by ArcGIS")
speciesCsv, countiesCsv = MossUtil.standardizeMossTables(inputCsv, outputFolder)

# Create a new geodatabase to contain new spatial data that we will create.
# If the geodatabase exists already, delete it and recreate it.
ws = os.path.join(outputFolder, OUTPUT_GDB)
arcpy.env.workspace = ws
if not arcpy.Exists(ws):
    arcpy.AddMessage("Creating new GDB")
    arcpy.CreateFileGDB_management(outputFolder, OUTPUT_GDB)
else:
    shutil.rmtree(ws)
    arcpy.AddMessage("Recreating GDB")
    arcpy.CreateFileGDB_management(outputFolder, OUTPUT_GDB)

# Read in the species table produced by standardizeMossTables as a  
# Pandas dataframe so that we can easily look up moss species attributes
arcpy.AddMessage("Creating species dataframe from CSV files")
try:
	species = pd.read_csv(speciesCsv, keep_default_na=False)
except:
	arcpy.AddError("Failed to create species table.")
	sys.exit(-1)
species.set_index(SPECIES_INDEX_NAME, inplace=True)

# Read in the moss presence table produced by standardizeMossTables as a
# geodatabase table so that we can easily join it to the county centroids
try:
	arcpy.TableToTable_conversion(countiesCsv, ws, COUNTIES_TABLE)
except:
	arcpy.AddError("Failed to create counties table.")
	sys.exit(-1)

# Copy the reference spatial data into geodatabase feature classes
arcpy.AddMessage("Copying spatial data to geodatabase for faster access.")
arcpy.CopyFeatures_management(BOUNDARIES_SHP, BOUNDARIES_FC)
arcpy.CopyFeatures_management(CENTROIDS_SHP, MOSSES_FC)

# arcpy.mp is unable to create map layouts from scratch.  Therefore 
# the layout for the maps is pre-defined in an ArcGIS project file.
# Load that file now.
try:
	proj = arcpy.mp.ArcGISProject(projectFile)
except:
	arcpy.AddError("Failed to load project file")
	sys.exit(-1)
arcpy.AddMessage("Using project file: " + proj.filePath)
	
# The project file must contain at least one map.  Use the first one in the list.
# This map should not contain any layers. The spatial reference (projection) used
# in the output maps will match the spatial reference specified for this map.
try:
	map = proj.listMaps()[0]
except IndexError:
	arcpy.AddError("No map exists in project file")
	sys.exit(-1)
arcpy.AddMessage("Using map: " + map.name)

# The project file must contain at least one layout.  Use the first one in the list.
# This layout defines the graphical elements that will be present on the output
# maps, as well as their properties and arrangement.  
try:
	lyt = proj.listLayouts()[0]
except IndexError:
	arcpy.AddError("No layout exists in project file")
	sys.exit(-1)
arcpy.AddMessage("Using layout: " + lyt.name)

# The layout must contain at least one map.  Use the one named "Map Frame".
# If more than one exists with that name, use the first one.  It is possible
# to specify the extent (or center and scale) for the output maps in the map
# frame properties, however we will compute this automatically later.  If you 
# want to specify it manually, comment out the mf.camera.setExtent line below.
try:	
	mf = lyt.listElements("mapframe_element", "Map Frame")[0]
except IndexError:
	arcpy.AddError("No map frame exists in layout")
	sys.exit(-1)
arcpy.AddMessage("Using map frame: " + mf.name)

# Add layers to map
arcpy.AddMessage("Creating map layer for county boundaries.")
try:
	boundaries_layer = map.addDataFromPath(os.path.join(os.getcwd(), ws, BOUNDARIES_FC))
except:
	arcpy.AddError("Failed to create map layer for county boundaries.")
	sys.exit(-1)
arcpy.AddMessage("Using boundary features from: " + boundaries_layer.dataSource)

arcpy.AddMessage("Creating map layer for county centroids (moss species data).")
try:
	mosses_layer = map.addDataFromPath(os.path.join(os.getcwd(), ws, MOSSES_FC))
except:
	arcpy.AddError("Failed to create map layer for county centroids (moss species data).")
	sys.exit(-1)
arcpy.AddMessage("Using centroid features from: " + mosses_layer.dataSource)

# At this point the mosses layer is just the county centroids and does not contain the
# attributes that indicate the presence of moss in the county.  Add these attributes by
# joining fields from the table we created earlier
arcpy.AddMessage("Joining moss presence information to centroids.")
arcpy.JoinField_management(mosses_layer, COUNTY_INDEX_NAME, COUNTIES_TABLE, COUNTY_INDEX_NAME)

# Create a list of fields whose names start with "SPECIES_".  These are the fields that
# contain moss presence information for each species of moss.  We will iterate through
# these fields one by one and create a map of each.
fields = arcpy.ListFields(mosses_layer, "SPECIES_*")
nRecords = len(fields)
arcpy.AddMessage("Processing maps for " + str(nRecords) + " records...")

# Set the visible map extent by finding the extent of the county boundaries polygons
extent = mf.getLayerExtent(boundaries_layer, False, False)
mf.camera.setExtent(extent)

# Specify the symbology of the county boundaries (uses SimpleRenderer by default)
boundaries_sym = boundaries_layer.symbology
boundaries_sym.renderer.symbol.applySymbolFromGallery("Black Outline")
boundaries_sym.renderer.symbol.size = 1
boundaries_layer.symbology = boundaries_sym

# Specify renderer for moss presence symbols.  Use UniqueValueRenderer which allows
# us to assign a different symbol for each unique value in the SPECIES_X field
mosses_sym = mosses_layer.symbology
mosses_sym.updateRenderer('UniqueValueRenderer')
mosses_layer.symbology = mosses_sym

# Loop through each of the moss species (SPECIES_X fields) and export a map for each one
for f in fields:
	# Extract the relevant attributes for the current moss species
	sName = species.loc[f.name, "ScientificName"].strip()
	sAuthority = species.loc[f.name, "Authority"].strip()
	sFamily = species.loc[f.name, "family"].strip()
	sSynonym = species.loc[f.name, "synonym"].strip()

	arcpy.AddMessage("Now mapping: " + f.name + " [" + sName + "/" + sAuthority + "]" )

	# A requirement for the map titles is that the scientific name and authority appear on the same line,
	# however the scientific name must be italicized and the authority must be unstyled.  ArcGIS layout
	# TEXT_ELEMENT objects do not allow for multiple styles in a single element. In cases where the combined
	# length of the scientific name and authority are too long to fit on a single line, the authority must
	# overflow onto a second line.  The next section of code breaks the authority line (if necessary) at the
	# appropriate length
	longString = sName + " " + sAuthority
	
	textLength = {
		'name': len(sName),
		'auth': len(sAuthority),
		'family': len(sFamily),
		'synonym': len(sSynonym),
		'longString': len(longString)
	}

	# If combined length of scientific name and authority exceeds the maximimum number of characters, we need
	# to break it to fit on two lines
	if(textLength['longString'] > TITLE_MAXCHAR):
		# Compute the number of characaters available for the authority
		i = TITLE_MAXCHAR - textLength['name'] - 1
		
		# Try to break the authority at a space.  Start at the maximimum text length and work backwards
		# until a space is found. Separate the contents occurring before and after the space into separate
		# strings
		while(i>0):
			if(sAuthority[i] == ' '):
				sAuthority = [sAuthority[0:i],sAuthority[i+1:]]
				longString = sName + " " + sAuthority[0]
				textLength['longString'] = len(longString)
				break
			else:
				i-=1
	
	mosses_sym = mosses_layer.symbology
	# Find the set of unique values present in the current SPECIES_X field
	# KNOWN ISSUE: This assignment may fail when the script is run from within ArcGIS Pro
	# as a script tool. The target variable will remain the same after the assignment as it 
	# was prior to the assignment (i.e. set to "County").  The cause of this is unknown.
	mosses_sym.renderer.fields = [f.name]

	# Assign a symbol for each of the unique values in the field, specifically
	# Null	--> (No symbol)
	# 1 	--> filled circle
	# 2		--> filled square
	for grp in mosses_sym.renderer.groups:
		for itm in grp.items:
			value = itm.values[0][0]
			if (value == "<Null>"):
				itm.label = "0 or N/A"
				itm.symbol.applySymbolFromGallery("Circle 2")
				itm.symbol.color = {'RGB' : [0, 0, 0, 0]}
				itm.symbol.outlineColor = {'RGB' : [0, 0, 0, 0]}
				itm.symbol.size = 10
			elif value == "1":
				itm.label = str(value)
				itm.symbol.applySymbolFromGallery("Circle 1")
				itm.symbol.color = {'RGB' : [0, 0, 0, 100]}
				itm.symbol.size = 9.75
				itm.symbol.outlineColor = {'RGB' : [0, 0, 0, 100]}
			elif value == "2":
				itm.label = str(value)
				itm.symbol.applySymbolFromGallery("Square 1")
				itm.symbol.color = {'RGB' : [0, 0, 0, 100]}
				itm.symbol.size = 8.75
				itm.symbol.outlineColor = {'RGB' : [0, 0, 0, 100]}

	# Apply the symbology change
	mosses_layer.symbology = mosses_sym
	
	# Compute the horizontal positions of the scientific name and authority 
	# TEXT_ELEMENT objects.  This assumes scientific name is right justified and 
	# authority is left justified.
	PAGEWIDTH = lyt.pageWidth
	# Leave a space of approximately one character between elements
	SPACER = PAGEWIDTH/TITLE_MAXCHAR  
	
	# Populate the "Name" text element with the scientific name and set its
	# position
	sNameText = lyt.listElements("TEXT_ELEMENT", "Name")[0]
	sNameText.text = sName
	
	# Compute the height of the element to determine vertical position of 
	# subsequent lines
	TITLE_HEIGHT = sNameText.elementHeight

	# Use an accumulator to keep track of incremental vertical position
	TITLE_YACC = 0
	
	# If authority is a list, then create another text element to capture the
	# overflow on another line
	if(type(sAuthority) == list):
		# Populate the "Authority" text element with the first part of the
		# authority and set its position
		sAuthorityText = lyt.listElements("TEXT_ELEMENT", "Authority")[0] # Assumes the element has the name "A"
		sAuthorityText.text = sAuthority[0]
		TITLE_YACC += 1
		# Populate the "Authority2" text element with the overflow, set
		# its position, and make it visible.
		sAuthorityText2 = lyt.listElements("TEXT_ELEMENT", "Authority2")[0]
		sAuthorityText2.visible = True
		sAuthorityText2.text = sAuthority[1]
		sAuthorityText2.elementPositionX = PAGEWIDTH/2  # Center it on the page
		sAuthorityText2.elementPositionY = TITLE_Y0 - TITLE_YACC * TITLE_HEIGHT
		TITLE_YACC += 1
	else:
		# Populate the "Authority" text element with the first part of the
		# authority and set its position
		sAuthorityText = lyt.listElements("TEXT_ELEMENT", "Authority")[0]
		sAuthorityText.text = sAuthority
		TITLE_YACC += 1
		# There is no overflow, so make the "Authority2" text element invisible
		sAuthorityText2 = lyt.listElements("TEXT_ELEMENT", "Authority2")[0]
		sAuthorityText2.visible = False		

	# Compute a horizontal coordinate to center the scientific name and authority
	# text elemenets on the page. Tip: the X axis runs rightward from the top left corner.
	TITLE_X0 = sNameText.elementWidth + (PAGEWIDTH - (sNameText.elementWidth + sAuthorityText.elementWidth + SPACER))/2
	sNameText.elementPositionX = TITLE_X0
	sNameText.elementPositionY = TITLE_Y0
	sAuthorityText.elementPositionX = TITLE_X0 + SPACER
	sAuthorityText.elementPositionY = TITLE_Y0
	
	# Populate the "Synonym" text element authority and set its position.
	# Make it visible only if it has a value. Vertical position is dependent 
	# on whether authority text overflowed.
	if(sSynonym != "" and sSynonym != None):
		if(type(sAuthority) == list):
			sSynonymText = lyt.listElements("TEXT_ELEMENT", "Synonym")[0]
			sSynonymText.text = "(" + sSynonym + ")"
			sSynonymText.elementPositionX = PAGEWIDTH/2
			sSynonymText.elementPositionY = TITLE_Y0 - TITLE_YACC * TITLE_HEIGHT
			TITLE_YACC += 1
			sSynonymText.visible = True
		else:
			sSynonymText = lyt.listElements("TEXT_ELEMENT", "Synonym")[0]
			sSynonymText.text = "(" + sSynonym + ")"
			sSynonymText.elementPositionX = PAGEWIDTH/2
			sSynonymText.elementPositionY = TITLE_Y0 - TITLE_YACC * TITLE_HEIGHT
			TITLE_YACC += 1
			sSynonymText.visible = True
	else:
		sSynonymText = lyt.listElements("TEXT_ELEMENT", "Synonym")[0]
		sSynonymText.visible = False

	# Populate the "Family" text element authority and set its position.
	# Vertical position is dependent on whether authority text overflowed.
	if(type(sAuthority) == list):
		sFamilyText = lyt.listElements("TEXT_ELEMENT", "Family")[0]
		sFamilyText.text = sFamily
		sFamilyText.elementPositionX = PAGEWIDTH/2
		sFamilyText.elementPositionY = TITLE_Y0 - TITLE_YACC * TITLE_HEIGHT
		TITLE_YACC += 1
	else:
		sFamilyText = lyt.listElements("TEXT_ELEMENT", "Family")[0]
		sFamilyText.text = sFamily
		sFamilyText.elementPositionX = PAGEWIDTH/2
		sFamilyText.elementPositionY = TITLE_Y0 - TITLE_YACC * TITLE_HEIGHT
		TITLE_YACC += 1	

	# Create output folder path based on output file extension (i.e. put
	# JPEG files in a "jpg" folder).  If the folder does not exist, create
	# it recursively.
	ext = fileType.lower()
	path = os.path.join(outputFolder, ext)			
	if (not os.path.exists(path)):
		try:
			os.makedirs(path)
		except:
			arcpy.AddError("Failed to create output path.")
			sys.exit(-1)

	# Specify the output filename based on he scientific name and the unique 
	# identifier for the record (i.e. the X from SPECIES_X).  Use the slugify
	# function (borrowed from Django) to make the string safe for filenames and
	# URLs
	filename = MossUtil.slugify(sName) + "." + ext

	# Create the output map image using the appropriate export driver
	if(fileType == "JPG"):
		lyt.exportToJPEG(os.path.join(path, filename), resolution=300, jpeg_quality=100) 
	elif(fileType == "PNG"):
		lyt.exportToPNG(os.path.join(path, filename), resolution=300)
	elif(fileType == "PDF"):
		lyt.exportToPDF(os.path.join(path, filename), resolution=300)
	else:
		arcpy.AddError("Output file type not recognized.")
		sys.exit(-1)
